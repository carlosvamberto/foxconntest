USE [LojaDB]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 16/07/2020 21:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Produto]    Script Date: 16/07/2020 21:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Produto](
	[ProdutoID] [bigint] IDENTITY(1,1) NOT NULL,
	[Nome] [nvarchar](30) NOT NULL,
	[Descricao] [nvarchar](100) NOT NULL,
	[Preco] [decimal](10, 2) NOT NULL,
 CONSTRAINT [PK_Produto] PRIMARY KEY CLUSTERED 
(
	[ProdutoID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200715144807_CriacaoDoBanco', N'3.1.6')
GO
SET IDENTITY_INSERT [dbo].[Produto] ON 

INSERT [dbo].[Produto] ([ProdutoID], [Nome], [Descricao], [Preco]) VALUES (1, N'Agua Mineral s/g', N'Agua minieral 500ml sem gás', CAST(1.15 AS Decimal(10, 2)))
INSERT [dbo].[Produto] ([ProdutoID], [Nome], [Descricao], [Preco]) VALUES (2, N'Agua Mineral c/g', N'Agua minieral 500ml com gás', CAST(1.25 AS Decimal(10, 2)))
INSERT [dbo].[Produto] ([ProdutoID], [Nome], [Descricao], [Preco]) VALUES (3, N'Coca Cola Lata', N'Coca Cola Lata 350ml', CAST(2.55 AS Decimal(10, 2)))
INSERT [dbo].[Produto] ([ProdutoID], [Nome], [Descricao], [Preco]) VALUES (4, N'Coca Cola Zero Lata', N'Coca Cola Zero Lata 350ml', CAST(2.63 AS Decimal(10, 2)))
INSERT [dbo].[Produto] ([ProdutoID], [Nome], [Descricao], [Preco]) VALUES (10, N'Feijão Preto 1kg Maroca', N'Feijão Preto 1k Maroca Exportação Super', CAST(71.00 AS Decimal(10, 2)))
SET IDENTITY_INSERT [dbo].[Produto] OFF
GO
/****** Object:  StoredProcedure [dbo].[SP_Incluir]    Script Date: 16/07/2020 21:17:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[SP_Incluir] 
                       (@nome nvarchar(30),
                        @descricao nvarchar(100),
                        @preco decimal(10,2),
                        @id bigint output)
                      AS
                        BEGIN
                            INSERT INTO dbo.Produto
                            (Nome,Descricao,Preco)
                            VALUES
                            (@nome,@descricao,@preco)

                            SET @id = SCOPE_IDENTITY()
                            RETURN @id
                        END
GO
