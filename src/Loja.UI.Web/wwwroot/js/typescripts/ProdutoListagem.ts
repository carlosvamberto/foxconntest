﻿
// Classe para tratar os eventos da página
class ProdutoListagemScript {

    // Pegando os componentes de tela
    btnAdicionar = $("#btnAdicionar");
    btnGravar = $("#btnGravar");
    btnSimExcluir = $("#btnSimExcluir");

    formulario = $("#formulario");

    formId = $("#formId");
    formNome = $("#formNome");
    formDescricao = $("#formDescricao");
    formPreco = $("#formPreco");

    txtMensagemErro = $("#txtMensagemErro");
    hdfAcao = $("#Acao");
    hdfIdExcluir = $("#IdExcluir");
    botoesAlterar = $(".btnsAlterar");
    botoesExcluir = $(".btnsExcluir");

    init(): void {

        // Evento AoClicar do botão btnAdicionar
        this.btnAdicionar.on("click", () => {
            // Zerando campos e colocando a Ação para Incluir
            this.hdfAcao.val("I");

            this.formId.val("0");
            this.formDescricao.val("");
            this.formNome.val("");
            this.formPreco.val("0");
        });

        // Envento AoClicar do botão btnGravar do modal 
        // de  Inclusão e Alteração
        this.btnGravar.on("click", () => {

            let mensagemErro = "";

            // Valida os campos
            if (this.hdfAcao.val() == "A") {
                if (this.formId.val() === "") {
                    mensagemErro += "<br/>Para realizar alteração, é necessário o ProdutoID";
                }
            }

            if (this.formNome.val() === "") {
                mensagemErro += "<br/>- O campo Nome não pode estar em branco";
            }

            if (this.formDescricao.val() === "") {
                mensagemErro += "<br/>- O campo Descricao não pode estar em branco";
            }

            if (this.formPreco.val() === "") {
                mensagemErro += "<br/>- O campo Preco não pode estar em branco";
            }

            if (mensagemErro.length > 0) {
                mensagemErro = "Erros encontrados: " + mensagemErro;
                this.txtMensagemErro.html(mensagemErro);
                // Exibe a mensagem de erro
                $("#divErro").fadeTo(2000, 500).slideUp(1000, () => {
                    $("#divErro").slideUp(500);
                });
            }
            else {
               
                this.formulario.submit();
            }
        });

        // Evento AoClicar dos botões btnsAlterar
        this.botoesAlterar.on("click", (e: JQueryEventObject) => {            
            this.hdfAcao.val("A"); // Alterar

            this.formId.val( $(e.currentTarget).data("id"));
            this.formNome.val($(e.currentTarget).data("nome"));
            this.formDescricao.val($(e.currentTarget).data("descricao"));
            this.formPreco.val($(e.currentTarget).data("preco"));
        });

        // Evento AoClicar dos botões btnsExcluir
        this.botoesExcluir.on("click", (e: JQueryEventObject) => {
            this.hdfAcao.val("E"); // Excluir
            this.hdfIdExcluir.val($(e.currentTarget).data("id"));
        });

        // Evento ÃoClicar do botão btnSimExcluir do modal Excluir
        this.btnSimExcluir.on("click", () => {
            this.formulario.submit();
        });
    }
}

// Ao Carregar a Página, executa o método init() 
// da Classe ProdutoListagemScript
window.onload = () => {
    const obj = new ProdutoListagemScript();
    obj.init();
}