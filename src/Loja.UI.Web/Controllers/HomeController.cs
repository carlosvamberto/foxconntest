﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Loja.UI.Web.Models;
using Loja.ApplicationCore.Interfaces.Services;
using Microsoft.Extensions.Logging;
using Loja.UI.Web.ViewModel;
using Loja.ApplicationCore.Entities;

namespace Loja.UI.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IProdutoService _produtoService;

        public HomeController(ILogger<HomeController> logger, IProdutoService produtoService)        
        {
            _logger = logger;
            _produtoService = produtoService;
        }

        public IActionResult Index()
        {
            ProdutoViewModel _viewModel = new ProdutoViewModel();

            _viewModel.ListaProduto = _produtoService.ObterTodos();

            return View(_viewModel);
        }

        [HttpPost]
        public IActionResult Index(ProdutoViewModel _viewModel)
        {          
            if (_viewModel.Acao == "I") // Incluir
            {
                try
                {
                    Produto _produto = new Produto()
                    {
                        Nome = _viewModel.formNome,
                        Descricao = _viewModel.formDescricao,
                        Preco = _viewModel.formPreco
                    };

                    long _id = _produtoService.AdicionarComProcedure(_produto);
                    _viewModel.MensagemSucesso = "Produto gravado com sucesso!";
                }
                catch (System.Exception ex)
                {
                    _viewModel.MensagemErro = "Erro ao tentar gravar o produto - \n" + ex.Message;
                }
            }
            else if (_viewModel.Acao == "A") // Alterar
            {
                try
                {
                    Produto _produto = new Produto()
                    {
                        ProdutoID = _viewModel.formId,
                        Nome = _viewModel.formNome,
                        Descricao = _viewModel.formDescricao,
                        Preco = _viewModel.formPreco
                    };
                    
                    _produtoService.Atualizar(_produto);
                    _viewModel.MensagemSucesso = "Produto gravado com sucesso!";
                }
                catch (System.Exception ex)
                {
                    _viewModel.MensagemErro = "Erro ao tentar gravar o produto - \n" + ex.Message;                    
                }                
            }
            else if (_viewModel.Acao == "E") // Excluir
            {
                try
                {
                    var _produto = _produtoService.ObterPorId(_viewModel.IdExcluir);
                    _produtoService.Remover(_produto);
                    _viewModel.MensagemSucesso = "Produto excluído com sucesso!";
                }
                catch (System.Exception ex)
                {
                    _viewModel.MensagemErro = "Erro ao tentar excluir o produto - \n" + ex.Message;
                }
            }

                _viewModel.ListaProduto = _produtoService.ObterTodos();

            return View(_viewModel);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
