using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Loja.ApplicationCore.Entities;
using Loja.ApplicationCore.Interfaces.Repository;
using Loja.ApplicationCore.Interfaces.Services;
using Loja.ApplicationCore.Services;
using Loja.Infrastructure.Data;
using Loja.Infrastructure.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Loja.UI.Web
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();

            // Adicionando Inje��o de Depend�ncia no servi�o
            services.AddTransient<IRepository<Produto>, Repository<Produto>>();
            services.AddTransient<IProdutoService, ProdutoService>();

            // Adicionando o contexto atrav�s de Inje��o de Depend�ncia
            services
                .AddDbContext<LojaContext>(opt => 
                    opt.UseSqlServer(Configuration.GetConnectionString("LojaConnection")));

            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
