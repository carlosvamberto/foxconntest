using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Loja.Infrastructure.Data;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Loja.UI.Web
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var host = CreateHostBuilder(args);
            
            // Seed para a carga inicial do banco de dados
            var build = host.Build();
            var scope = build.Services.CreateScope();
            var services = scope.ServiceProvider;
            var context = services.GetRequiredService<LojaContext>();
            DbInitializer.Initializer(context);
            //Fim seed

            build.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                });
    }
}
