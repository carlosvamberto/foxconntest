﻿using Loja.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Loja.UI.Web.ViewModel
{
    public class ProdutoViewModel
    {
        public IEnumerable<Produto> ListaProduto { get; set; }

        public string FiltroProduto { get; set; }
        public string Acao { get; set; } = "N"; // I-Incluir A-Alterar N-Neutro 
        public long IdExcluir { get; set; } = 0;

        // Campos de Inclusão e Alteração
        public long formId { get; set; }
        public string formNome { get; set; }
        public string formDescricao { get; set; }
        public decimal formPreco { get; set; }

        // Mensagems de Erro ou de Sucesso
        public string MensagemErro { get; set; } = "";
        public string MensagemSucesso { get; set; } = "";

    }
}
