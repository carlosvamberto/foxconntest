﻿using Loja.ApplicationCore.Entities;
using Loja.ApplicationCore.Interfaces.Repository;
using Loja.Infrastructure.Data;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Loja.Infrastructure.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {        
        protected readonly LojaContext _context;

        // Construtor para usar a Injeção de Dependência
        // Para atribui o contexto
        public Repository(LojaContext context)
        {
            _context = context;
        }

        public T Adicionar(T entity)
        {
            _context.Set<T>().Add(entity);
            _context.SaveChanges();
            return entity;
        }

        public long AdicionarComProcedure(Produto entity)
        {
            var sql = "exec SP_Incluir @nome, @descricao, @preco, @id OUT";            

            var parNome = new SqlParameter
            {
                ParameterName = "@nome",
                DbType = System.Data.DbType.String,
                Direction = System.Data.ParameterDirection.Input,
                Value = entity.Nome
            };

            var parDescricao = new SqlParameter
            {
                ParameterName = "@descricao",
                DbType = System.Data.DbType.String,
                Direction = System.Data.ParameterDirection.Input,
                Value = entity.Descricao
            };

            var parPreco = new SqlParameter
            {
                ParameterName = "@preco",
                DbType = System.Data.DbType.Decimal,
                Direction = System.Data.ParameterDirection.Input,
                Value = entity.Preco
            };

            var parId = new SqlParameter
            {
                ParameterName = "@id",
                DbType = System.Data.DbType.Int64,
                Direction = System.Data.ParameterDirection.Output
            };

            _context.Database.ExecuteSqlCommand(sql, parNome, parDescricao, parPreco, parId);

            return long.Parse(parId.Value.ToString());
        }

        public void Atualizar(T entity)
        {
            _context.Entry(entity).State = Microsoft.EntityFrameworkCore.EntityState.Modified;            
            _context.SaveChanges();
        }

        public IEnumerable<T> Buscar(Expression<Func<T, bool>> predicado)
        {
            return _context
                .Set<T>()
                .Where(predicado)
                .AsEnumerable();
        }

        public T ObterPorId(long id)
        {
            return _context.Set<T>().Find(id);
        }

        public IEnumerable<T> ObterTodos()
        {
            return _context.Set<T>().AsEnumerable();
        }

        public void Remover(T entity)
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }
    }
}
