﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Loja.Infrastructure.Migrations
{
    public partial class AdicionarStoreProcedure : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            string _sp_incluir = @"
                        USE LojaDB;

                        CREATE PROCEDURE dbo.SP_Incluir 
                       (@nome nvarchar(30),
                        @descricao nvarchar(100),
                        @preco decimal(10,2),
                        @id bigint output)
                      AS
                        BEGIN
                            INSERT INTO dbo.Produto
                            (Nome,Descricao,Preco)
                            VALUES
                            (@nome,@descricao,@preco)

                            SET @id = SCOPE_IDENTITY()
                            RETURN @id
                        END";
            migrationBuilder.Sql(_sp_incluir);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.Sql("DROP PROCEDURE dbo.SP_Incluir");
        }
    }
}
