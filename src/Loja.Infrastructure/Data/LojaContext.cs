﻿using Loja.ApplicationCore.Entities;
using Loja.Infrastructure.EntityConfig;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Loja.Infrastructure.Data
{
    public class LojaContext : DbContext
    {
        /// <summary>
        /// Configuração do Contexto do EF
        /// </summary>
        /// <param name="options"></param>
        public LojaContext(DbContextOptions<LojaContext> options) : base(options)
        {

        }

        #region Configuração as Entidades
        public DbSet<Produto> Produtos { get; set; }

        #endregion

        /// <summary>
        /// Configurações na Criação das Tabelas
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Carrega as configurações da entidade Produto
            modelBuilder.ApplyConfiguration(new ProdutoMap());
        }
    }
}
