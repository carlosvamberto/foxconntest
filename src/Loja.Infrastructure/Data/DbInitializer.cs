﻿using Microsoft.EntityFrameworkCore.Internal;
using System.Linq;
using Loja.ApplicationCore.Entities;

namespace Loja.Infrastructure.Data
{
    public static class DbInitializer
    {
        public static void Initializer(LojaContext context)
        {
            if (context.Produtos.Any())
            {
                return;
            }

            context.Produtos.Add(new Produto()
            {
                Nome = "Agua Mineral s/g",
                Descricao = "Agua minieral 500ml sem gás",
                Preco = 1.15M
            });

            context.Produtos.Add(new Produto()
            {
                Nome = "Agua Mineral c/g",
                Descricao = "Agua minieral 500ml com gás",
                Preco = 1.25M
            });

            context.Produtos.Add(new Produto()
            {
                Nome = "Coca Cola Lata",
                Descricao = "Coca Cola Lata 350ml",
                Preco = 2.55M
            });

            context.Produtos.Add(new Produto()
            {
                Nome = "Coca Cola Zero Lata",
                Descricao = "Coca Cola Zero Lata 350ml",
                Preco = 2.63M
            });

            context.SaveChanges();
        }
            
    }
}
