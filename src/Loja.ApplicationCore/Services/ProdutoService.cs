﻿using Loja.ApplicationCore.Entities;
using Loja.ApplicationCore.Interfaces.Repository;
using Loja.ApplicationCore.Interfaces.Services;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Loja.ApplicationCore.Services
{
    public class ProdutoService : IProdutoService
    {
        IRepository<Produto> _produtoRepository;
        public ProdutoService(IRepository<Produto> produtoRepository)
        {
            _produtoRepository = produtoRepository;
        }

        public long AdicionarComProcedure(Produto entity)
        {
            return _produtoRepository.AdicionarComProcedure(entity);
        }

        public virtual Produto Adictionar(Produto entity)
        {            
            return _produtoRepository.Adicionar(entity);
        }

        public void Atualizar(Produto entity)
        {
            _produtoRepository.Atualizar(entity);
        }

        public IEnumerable<Produto> Buscar(Expression<Func<Produto, bool>> predicado)
        {
            return _produtoRepository.Buscar(predicado);
        }

        public Produto ObterPorId(long id)
        {
            return _produtoRepository.ObterPorId(id);
        }

        public IEnumerable<Produto> ObterTodos()
        {
            return _produtoRepository.ObterTodos();
        }

        public void Remover(Produto entity)
        {
            _produtoRepository.Remover(entity);
        }
    }
}
