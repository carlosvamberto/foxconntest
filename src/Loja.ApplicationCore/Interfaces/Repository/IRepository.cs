﻿using Loja.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Loja.ApplicationCore.Interfaces.Repository
{
    public interface IRepository<T> where T : class
    {
        T Adicionar(T entity);
        long AdicionarComProcedure(Produto entity);
        void Atualizar(T entity);
        IEnumerable<T> ObterTodos();
        T ObterPorId(long id);
        IEnumerable<T> Buscar(Expression<Func<T, bool>> predicado);
        void Remover(T entity);
    }
}
