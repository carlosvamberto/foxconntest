﻿using Loja.ApplicationCore.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Loja.ApplicationCore.Interfaces.Services
{
    public interface IProdutoService
    {
        Produto Adictionar(Produto entity);
        long AdicionarComProcedure(Produto entity);
        void Atualizar(Produto entity);
        IEnumerable<Produto> ObterTodos();
        Produto ObterPorId(long id);
        IEnumerable<Produto> Buscar(Expression<Func<Produto, bool>> predicado);
        void Remover(Produto entity);
    }
}
