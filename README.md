# FoxConnTest

## Feito por 
- Carlos Vamberto Filho

## IDE usada
- Visual Studio 2019 Community

## Tecnologias usadas
- ASP.NET Core 3.1
- Entity Framework Core
- TypeScript
- SQL Server 2019 Express

## Arquiteturas
- Clean DDD
- Generic Repository
- Fonte [Architecting Modern Web Application with ASP.NET Core and Microsoft Azure](https://docs.microsoft.com/pt-br/dotnet/architecture/modern-web-apps-azure/)

